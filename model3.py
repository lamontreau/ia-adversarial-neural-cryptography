import warnings
warnings.simplefilter("ignore")
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.layers import Embedding, Flatten, Input, Reshape
from tensorflow.keras.layers import Conv1D, GlobalAveragePooling1D, MaxPooling1D
from Crypto.Cipher import AES
EPOCHS=1000

optimizer=tf.keras.optimizers.Adam(learning_rate=0.08)

n=0
seq_length = 17
#x=iter([0,1,0,0,0,1,1,1,0,0,0,1,0,1,0,0,0])
#y=iter([0,1,1,1,0,0,0,1,0,1,1,1,0,0,0,1,0])

y=[0,1,1,1,0,0,0,1,0,1,1,1,0,0,0,1,0]

bob = Sequential()
bob.add(Input((seq_length,)))
bob.add(Reshape((seq_length, 1)))
#bob.add(Reshape((seq_length, 1), input_shape=(seq_length,)))
bob.add(Conv1D(64, 3, activation='relu'))
#bob.add(Conv1D(64, 3, activation='relu'))
bob.add(MaxPooling1D(3))
bob.add(Conv1D(128, 3, activation='relu'))
bob.add(Conv1D(128, 3, activation='relu'))
bob.add(GlobalAveragePooling1D())
bob.add(Dropout(0.5))
bob.add(Dense(1, activation='sigmoid'))

alice = Sequential()
alice.add(Input((seq_length,)))
alice.add(Reshape((seq_length, 1)))
alice.add(Conv1D(64, 3, activation='relu'))
#alice.add(Conv1D(64, 3, activation='relu'))
alice.add(MaxPooling1D(3))
alice.add(Conv1D(128, 3, activation='relu'))
alice.add(Conv1D(128, 3, activation='relu'))
alice.add(GlobalAveragePooling1D())
alice.add(Dropout(0.5))
alice.add(Dense(1, activation='sigmoid'))

loss_fn=tf.keras.losses.SparseCategoricalCrossentropy()

for i in range(EPOCHS):
    with tf.GradientTape() as tape:
        aliceout=alice(tf.cast(np.array([y]), tf.float32))
        bobout=bob(tf.cast(np.array([y]), tf.float32))
        loss=loss_fn(aliceout,tf.cast(np.array([y]), tf.float32))
        loss2=loss_fn(bobout,tf.cast(np.array([y]), tf.float32))

print(loss)
print(loss2)